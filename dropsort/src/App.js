import React, { Component } from 'react';
import './App.css';
import {Button} from 'react-bootstrap';
import {SignIn} from './components/signIn';
import {ListFiles} from './components/listFiles';


class App extends Component {
  constructor(props){
    super(props);
    this.state = {};
    this.state.files = [];
  }
  
   componentDidMount(){
    let files =  this.getAllFiles();
    this.setState({
      files : files
    });
    console.log(files);
  }
  
  getAllFiles =  () => {
   /* let res = await fetch('http://seminar4-ioanatrandafir97.c9users.io/get-all-documents/:1',{mode: 'cors'});
    let files = res.json();
    return files*/
    let files = [{
      name : "Picture.jpg",
      folder : "Images",
      path : "/Images/Picture.jpg",
      size : 20
      },
                {
      name : "TechProject.doc",
      folder : "Projects",
      path : "/Projects/TechProject.doc",
      size : 120
      },
                {
      name : "Econometrie.doc",
      folder : "Projects",
      path : "/Projects/Econometrie.doc",
      size : 225      
      },
                {
      name : "Flower.jpg",
      folder : "Images",
      path : "/Images/Flower.jpg",
      size : 90              
                }
                         ];
      return files;
      
            
  }
  
  render() {
    
    return (
      <React.Fragment>
        <ListFiles source={this.state.files}/>
      </React.Fragment>
      
    );
  }
}

export default App;
