import React  from 'react';
import {Button} from 'react-bootstrap';




export class SignIn extends React.Component{
    
    render(){
        return (
            <React.Fragment>
                <div className="logo"></div>
                <div className="container">
                    DropSort
                    <Button onClick={this.handleSignIn} id="signInButton" bsSize="large">Sign In</Button>
                </div>
            </React.Fragment>
            );
    }
}