import React  from 'react';
import {Button} from 'react-bootstrap';

export class ListFiles extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            criteria : "None",
            text : ""
        };
        
    }
    
    listFiles = () => {
        if(this.state.criteria === "None" || this.state.text === ""){
             return this.props.source.map((file,index) => {
                return <div key={index}>{file.name}</div>;});
                
        }
        
        switch(this.state.criteria){
                case "Folder" : { console.log(this.state.text); return this.props.source.filter(file => file.folder === this.state.text).map((file, index) => {return <div key={index}>{file.name}</div>;})}
                case "Extensie" : this.props.source.filter(file => file.name.split('.').pop() === this.state.text).map((file,index) => {return <div key={index}>{file.name}</div>;});
                case "Nume" : this.props.source.filter(file => file.name.includes(this.state.text)).map((file,index) => {return <div key={index}> {file.name} </div>;});
                default : return this.props.source.map((file,index) => { return <div key={index}>{file.name}</div>;});
            
            }
        }    

    
    
    
    
    handleOnChangeSelect = () => {
        var select = document.getElementById("selectClasificare");
        this.setState({criteria :select.options[select.selectedIndex + 1].value});
        console.log(this.state.criteria);
    }
    
    handleOnChangeInput = () => {
        var input = document.getElementById("inputClasificare");
        this.setState({text : input.value});
        console.log(input.value);
        this.listFiles();
    }
    
    render(){
        let files =this.listFiles();
        return(
            <React.Fragment>
                <h1 id="titluLista">Clasificare Documente</h1>
                <div className="logo"></div>
                <div className="state">
                <select id="selectClasificare" onChange={this.handleOnChangeSelect}>
                    <option value="Folder">Fisierul se regaseste in folderul</option>
                    <option value="Nume">Numele fisierului contine cuvantul</option>
                    <option value="Extensie">Fisierul are extensia</option>
                    <option value="None">Nicio filtrare</option>
                </select>
                <input type="text" id="inputClasificare" onChange={this.handleOnChangeInput} placeholder="Introduceti criteriul de clasificare" />
                </div>
                <div className="list-container">
                    {files}
                   
                </div>
            </React.Fragment>
            );
    }
}