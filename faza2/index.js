const express = require('express');
const bodyParser = require("body-parser");
const Sequelize = require("sequelize");

const app = express();
app.use(bodyParser.json());

const connection = new Sequelize('c9', 'ioanatrandafir97', '', {
    host:'localhost',
    dialect:'mysql',
    operatorsAliases: false,
    pool: {
        "max": 1,
        "min": 0,
        "idle": 200000,
        "acquire": 200000
    }
});

connection.authenticate()
    .then(() => {
        console.log('Connection to the database has been established!');
    })
    .catch((err) =>{
        console.log('Unable to connect to the database!');
    });
    

//Define the User Model
const User = connection.define('users', {
    firstname: {
        type: Sequelize.STRING,
        allowNull: false,
        validate : {
            len : [3,100]
        }
    },
    surname: {
        type: Sequelize.STRING,
        allowNull: false,
         validate : {
            len : [3,100]
        }
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        validate : {
            isEmail : {
                msg : 'Please enter a valid Email Address'
            }
        }
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
        }
    
    }, {
        timestamps: false
    });


const Folder=connection.define('folders',{
    id:{
        type:Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name:{
        type:Sequelize.STRING,
        allowNull:false
    },
    size:{
        type:Sequelize.DOUBLE,
        allowNull:false
    },
    path:{
        type:Sequelize.STRING,
        allowNull:false,
        unique:true
    },
   
    
});

Folder.belongsTo(User);

//Define the Document Model(Data crearii si a ultimei modificari vor fi create automat)
const Document = connection.define('documents',{
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    extension:{
        type: Sequelize.STRING
    },
    path:{
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    content:{
        type: Sequelize.BLOB,
        allowNull: false
    },
    size:{
        type:Sequelize.DOUBLE,
        allowNull:false
    }},
    
    
);

Folder.hasMany(Document);


const Permission=connection.define('permission',{
    id:{
        type:Sequelize.INTEGER,
        primaryKey: true, 
        autoIncrement: true
    },
    type_permission:{
        type:Sequelize.STRING,
        allowNull:false
    }},
    {
        timestamps: false
    });
const PermissionDocument=connection.define('permissionDocument',{
    is_owner:{
        type:Sequelize.BOOLEAN,
        allowNull:false,
        defaultValue: false
    }}, {
        timestamps: false
    });
    
User.belongsToMany(Document, { through: PermissionDocument, primaryKey:true });
PermissionDocument.belongsTo(Permission);
Document.belongsToMany(User, {through: PermissionDocument, primaryKey:true});

connection.sync({
    force:true, //pentru a recrea tabelele
    logging: console.log
}).then(() =>{
    console.log('Tabels were successfully created!');
}).catch((error) => {
    console.log(error);
});

//Metoda: POST
//Cale: /register
//Aceasta metoda inregistreaza un nou utilizator(adauga un utilizator in tabela users)
app.post('/register', (req, res) =>{
    User.create({
        firstname: req.body.firstname,
        surname: req.body.surname,
        email: req.body.email,
        password: req.body.password
    }).then((user) => {
        res.status(201).send(user);
    })
    .catch((err) => {
        res.status(500).send(err);
    })
});

//Metoda: GET
//Cale: /users
//Aceasta metoda va afisa toti utilizatorii
app.get('/users', (req, res) =>{
    User.findAll()
    .then((users) => res.status(200).send(users))
    .catch((err) => res.status(500).send(err))
    
});

//Metoda: POST
//Cale: /login
//Aceasta metoda verifica daca user-ul primit in body se regaseste in tabela users
app.post('/login', (req, res) => {
    User.findOne({
        where: {
            email: req.body.email,
            password: req.body.password
        }
    })
    .then((result) => {
        res.status(201).send(result);
    })
    .catch((error) => {
        console.log(error);
        res.status(500).json({message: 'Server error'});
    })
});

//Metoda: POST
//Cale: /create-document
//Aceasta metoda va crea un nou document
app.post('/create-document', (req, res) => {
    Document.create({
        name: req.body.name,
        extension: req.body.extension,
        path: req.body.path,
        size: req.body.size,
        content: req.body.content,
        folderId: req.body.folderId
    })
    .then((result) => {
        res.status(201).send(result)
    })
     .catch((error) => {
        console.log(error);
        res.status(500).send(error);
    })
})

//Metoda: GET
//Cale: /get-all-documents
//Aceasta metoda va afisa utilizatorului toate documentele existente(utilizatorul are cel putin permisiune de a le vedea)
app.get('/get-all-documents/:id', (req, res) => {
    var ids = [];
    PermissionDocument.findAll({where: {
        userId: req.params.id
    }}).then((permissionDocuments) => {
        for(var i=0 ;i<permissionDocuments.length; i++){
            ids.push(permissionDocuments[i]['documentId']);
            console.log(permissionDocuments[i]['documentId']);
        }}).then(() => {
            Document.findAll({
                         where: {
                             id : ids
                                }
                 })
                .then((documents) => res.status(200).send(documents))
                .catch((error) => {
                     console.log(error);
                     res.status(500).json({message: 'Server error'});
                })
             })
    
    
});


//Metoda: GET
//Cale: /documents-in-folder
//Aceasta metoda va afisa utilizatorului toate documentele existente in folderul dat in query params
app.get('/documents-in-folder/:id', (req, res) => {
    Document.findAll({where: {
        folderId: req.params.id
    }})
    .then((documents) => res.status(200).send(documents))
    .catch((error) => {
        console.log(error);
        res.status(500).json({message: 'Server error'});
    })
});



app.post('/add-folder',(req,res)=>{//Method:POST,Path:'/add-folder',create folder
    Folder.create({
        name:req.body.name,
        size:req.body.size,
        path:req.body.path,
        userId: req.body.userId
    }).then((folder)=>{
        res.status(201).send(folder);
    },(err)=>{
        res.status(500).send(err);
    });
});

app.get('/get-all-permissions',(req,res)=>{//Method:GET,Path:'/get-all-permissions',gets all permissions
    Permission.findAll().then((permissions)=>{
        res.status(200).send(permissions);
    });
});


app.post('/add-permission',(req,res)=>{//Method:POST,Path:'/add-permission',create permission
    Permission.create({
        type_permission:req.body.type_permission
    }).then((result)=>{
        res.status(200).send("created successfully");
    },(err)=>{
        res.status(500).send(err);
    });
});


app.put('/update-folder/:id', async (req, res)=>{//Method:PUT,Path:'/update-folder',update folder with the content from the request body
    try{
        let folder =await Folder.findById(req.params.id);
        if(folder){
            await folder.update(req.body);
            res.status(200).send('The folder was successfully updated');
        }
        else{
            res.status(404).send('The folder with the given id was not found');
        }
    }
    catch(error){
        res.status(500).send(error);
    }
});

//Method: PUT
//PATH: update-document/:id
//The method will update the document with the give id (if it exists) with the content from the body request
app.put('update-document/:id', async (req, res) => {
   try{
       let doc = await Document.findById(req.params.id);
       if(doc){
           await doc.update(req.body);
           res.status(200).send('The document was successfully updated');
       }
       else{
           res.status(404).send('The document with the given is was not found');
       }
   } 
   catch(error){
       res.status(500).send(error);
   }
});

//Method:DELETE,Path:'/delete-permission/:id',destroy permission with the given id
app.delete('/delete-permission/:id',async (req, res)=>{
    try{
        let permission =await Permission.findById(req.params.id);
    if(permission){
        await permission.destroy();
        res.status(200).send('Permission was deleted');
    }
    else{
        res.status(404).send('Permission was not found');
        }
    }
    catch(error){
        res.status(500).send('Server error');
    }
});


//Method:DELETE,Path:'/delete-document/:id', This method will delete the document at the given id
//First it will search for the document with the id from the params, and if it is found then it will be deleted
app.delete('/delete-document/:id', async (req, res) => {
    try{
        let doc = await Document.findById(req.params.id);
        if(doc){
            await doc.destroy();
            res.status(200).send('Document was successfully deleted');
        }
        else{
            res.status(404).send('Document was not found');
        }
    }
    catch(error){
        res.status(500).send('Server error');
    }
});

//Method: DELETE
//Path: /delete-folder/:id
//This method will delete a folder with the given id(if exists)
app.delete('/delete-folder/:id',async (req, res) => {
    try{
        let folder = await Folder.findById(req.params.id);
        if(folder){
            await folder.destroy();
            res.status(200).send('Folder was successfully deleted');
        }
        else{
            res.status(404).send('Folder was not found');
        }
    }
    catch(error){
        res.status(500).send('Server error');
    }
    
    
})


app.post('/add-permission-document',(req,res)=>{//Method:POST,Path:'/add-permission-document',create permission-document
    PermissionDocument.create({
        userId : req.body.userId,
        documentId : req.body.documentId,
        permissionId: req.body.permissionId,
        is_owner:req.body.is_owner
    }).then((folder)=>{
        res.status(200).send("created successfully");
    },(err)=>{
        res.status(500).send(err);
    });
});

app.get('/get-all-folders/:id',(req,res)=>{//Method:GET,Path:'/get-all-folders',select folders that belong to a user
    Folder.findAll({
        where: {
            userId: req.params.id
        }
    }).then((folders)=>{
        res.status(200).send(folders);
    });
});

//Metoda: GET
//Path: /get-folders-and-documents/:id
//This method will get all the folders and the documents related to each folder
app.get('/get-folders-and-documents/:id', async (req, res)=>{
    try{
        let folders = await Folder.findAll({
            where:{
                userId: req.params.id
            },
            include: {model: Document}
        });
        res.status(200).send(folders);
    }
    catch(error){
        res.status(500).send(error);
        console.log(error);
    }
})



app.listen(8080, ()=>{
    console.log('Server started on port 8080...');
})








